import java.util.ArrayList;

/**
 * Represent a Talkative person.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
public class Talkative extends Person {
	/**
    * Contain the list of real Talkative persons.
    */
	public static ArrayList<Talkative> talkatives = new ArrayList<Talkative>();
	
	/**
 	* Construct three Talkative people, to use them in the example program.
 	* <p>talkative1 & talkative1password, talkative2 & talkative2password, talkative3 & talkative3password</p>
 	*
 	* @param  args Given arguments at the program (useless for this function)
 	* @see         Main
 	* @see         Person
 	*/
	public static void main(String args[]) {
		new Talkative("talkative1","talkative1password",true);
		Talkative t2=new Talkative("talkative2","talkative2password",true);
		Talkative t3=new Talkative("talkative3","talkative3password",true);
		t3.messageBox.add(new Message("message1Subject","message1Body",t2,t3));
		t2.messageBox.add(new Message("message2Subject","message2Body",t3,t2));
	}

	/**
 	* Construct a temporary Talkative person.
 	*
 	* @param  user     The pseudo of the Talkative person
 	* @param  password The password of the Talkative person
 	* @see             Person
 	*/
	public Talkative(String user,String password) {
		super(user,password);
	}
	/**
 	* Construct a Talkative person.
 	*
 	* @param  user     The pseudo of the Talkative person
 	* @param  password The password of the Talkative person
 	* @param  isReal   Determine if it is a temporary or real Talkative person
 	* @see             Person
 	*/
	public Talkative(String user,String password,boolean isReal) {
		super(user,password,isReal);
		if(isReal) {
			Talkative.talkatives.add(this);
		}
	}

	/**
 	* A Talkative person can only send to a Concierge person.
 	*
 	* @param  message  The message to send
 	* @see             MessageEvent
 	* @see             Person
 	*/
	@Override public void emit(Message message) {
		if(message.to instanceof Concierge)
			super.emit(message);
		else
			super.emit(new Message("Message not sent","You can send a message only to a Concierge... Sorry !",this,this));
	}
}
