/**
 * Determine if a class can send a message.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
public interface MessageEvent {
	/**
 	* Determine if a class can send a message.
 	*
 	* @param  message  The message to send.
 	* @see             Person
 	* @see             Talkative
 	* @see             Concierge
 	*/
	public void emit(Message message);
}
