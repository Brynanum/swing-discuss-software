import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;

import javax.swing.border.LineBorder;
import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Contains all concerned code about the graphical interface.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
public class GUI extends JFrame {
	/**
	* The identifiant of the class.
	*/
	private static final long serialVersionUID = 7526472295622776147L;
	/**
	* The main zone of the window.
	*/
	private JPanel contentPane;
	/**
	* The field of the user connection.
	*
	* @see Connection
	*/
	private JTextField connectionUserField;
	/**
	* The field of the password connection.
	*
	* @see Connection
	*/
	private JTextField connectionPasswordField;
	/**
	* The field of the password update.
	*
	* @see Connection
	*/
	private JTextField userPasswordField;
	/**
	* The field of the user update.
	*
	* @see Connection
	*/
	private JTextField userUserField;
	/**
	* The field of the subject message form.
	*
	* @see Message
	*/
	private JTextField messageFormSubjectField;
	/**
	* The field of the body message form.
	*
	* @see Message
	*/
	private JTextField messageFormUserField;

	/**
	 * Display the graphical interface.
	 *
 	 * @param  args Given arguments at the program (useless for this function)
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the graphical interface.
	 */
	public GUI() {
		setTitle("Bavard");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		JPanel window = new JPanel();
		contentPane.add(window);
		window.setLayout(null);
		
		// =================== USER ==========================
		// Components
		JPanel user = new JPanel();
		user.setBorder(new LineBorder(new Color(0, 0, 0)));
		user.setBounds(324, 12, 550, 100);
		window.add(user);
		user.setLayout(null);
		
		JLabel userUserLabel = new JLabel("User :");
		userUserLabel.setEnabled(false);
		userUserLabel.setBounds(12, 14, 60, 15);
		user.add(userUserLabel);
		
		JLabel userPasswordLabel = new JLabel("Password :");
		userPasswordLabel.setEnabled(false);
		userPasswordLabel.setBounds(12, 39, 80, 15);
		user.add(userPasswordLabel);
		
		userPasswordField = new JTextField();
		userPasswordField.setEnabled(false);
		userPasswordField.setColumns(10);
		userPasswordField.setBounds(108, 37, 180, 19);
		user.add(userPasswordField);
		
		userUserField = new JTextField();
		userUserField.setEnabled(false);
		userUserField.setColumns(10);
		userUserField.setBounds(108, 12, 180, 19);
		user.add(userUserField);
		
		JLabel userInfoLabel = new JLabel("");
		userInfoLabel.setForeground(Color.RED);
		userInfoLabel.setBounds(243, 66, 173, 15);
		user.add(userInfoLabel);
		
		JCheckBox userConciergeField = new JCheckBox("is a Concierge");
		userConciergeField.setEnabled(false);
		userConciergeField.setBounds(108, 64, 118, 23);
		user.add(userConciergeField);
		
		// Components with events
		JButton userCreateEventButton = new JButton("Create");
		userCreateEventButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				userInfoLabel.setText("");
				for(Person p : Person.people)
					if(p.connection.user.equals(userUserField.getText()))
						userInfoLabel.setText("User already exists !");
				if(userInfoLabel.getText()=="") {
					if(userConciergeField.isSelected())
						new Concierge(userUserField.getText(),userPasswordField.getText(),true);
					else
						new Talkative(userUserField.getText(),userPasswordField.getText(),true);
					userInfoLabel.setText("User created !");
				}
			}
		});
		userCreateEventButton.setEnabled(false);
		userCreateEventButton.setBounds(432, 4, 106, 25);
		user.add(userCreateEventButton);
		
		JButton userDeleteEventButton = new JButton("Delete");
		userDeleteEventButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				userInfoLabel.setText("");
				Person thePerson=null;
				for(Person p : Person.people)
					if(p.connection.user.equals(userUserField.getText()))
						thePerson=p;
				if(thePerson==null) {
					userInfoLabel.setText("User not exists !");
				} else {
					Person.people.remove(thePerson);
					userInfoLabel.setText("User deleted !");
				}
			}
		});
		userDeleteEventButton.setEnabled(false);
		userDeleteEventButton.setBounds(432, 34, 106, 25);
		user.add(userDeleteEventButton);
		
		JButton userUpdateEventButton = new JButton("Update");
		userUpdateEventButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				userInfoLabel.setText("");
				for(Talkative t : Talkative.talkatives) {
					if(t.connection.user.equals(userUserField.getText())) {
						if(userConciergeField.isSelected()) {
							new Concierge(userUserField.getText(),userPasswordField.getText(),true);
							Talkative.talkatives.remove(t);
						} else
							t.modify(userUserField.getText(),userPasswordField.getText());
						userInfoLabel.setText("User updated !");
						break;
					}
				}
				if(userInfoLabel.getText()=="") {
					for(Concierge c : Concierge.concierges) {
						if(c.connection.user.equals(userUserField.getText())) {
							if(!userConciergeField.isSelected()) {
								new Talkative(userUserField.getText(),userPasswordField.getText(),true);
								Concierge.concierges.remove(c);
							} else
								c.modify(userUserField.getText(),userPasswordField.getText());
							userInfoLabel.setText("User updated !");
							break;
						}
					}
				}
				if(userInfoLabel.getText()=="") {
					userInfoLabel.setText("User not exists !");
				}
			}
		});
		userUpdateEventButton.setEnabled(false);
		userUpdateEventButton.setBounds(432, 63, 106, 25);
		user.add(userUpdateEventButton);
		
		// =================== MESSAGE ==========================
		// Components
		JPanel message = new JPanel();
		message.setBorder(new LineBorder(new Color(0, 0, 0)));
		message.setBounds(12, 124, 862, 282);
		window.add(message);
		message.setLayout(null);
		
		JLabel messageSubjectLabel = new JLabel("");
		messageSubjectLabel.setEnabled(false);
		messageSubjectLabel.setBounds(294, 13, 370, 15);
		message.add(messageSubjectLabel);
		
		JLabel messageAuthorLabel = new JLabel("");
		messageAuthorLabel.setEnabled(false);
		messageAuthorLabel.setBounds(676, 13, 174, 15);
		message.add(messageAuthorLabel);
		
		JLabel messageBodyLabel = new JLabel("");
		messageBodyLabel.setEnabled(false);
		messageBodyLabel.setBounds(294, 40, 556, 230);
		message.add(messageBodyLabel);
		
		// Components with events
		JList<Message> messageList = new JList<Message>();
		messageList.addMouseListener(new MouseAdapter() {
			@Override public void mouseClicked(MouseEvent e) {
				Message m=(Message) messageList.getSelectedValue();
				messageSubjectLabel.setText(m.subject);
				messageAuthorLabel.setText(m.from.connection.user);
				messageBodyLabel.setText("<html>"+m.body+"</html>");
			}
		});
		messageList.setEnabled(false);
		messageList.setBounds(12, 12, 264, 258);
		message.add(messageList);
		
		// =================== MESSAGE FORM ==========================
		// Components
		JPanel messageForm = new JPanel();
		messageForm.setBorder(new LineBorder(new Color(0, 0, 0)));
		messageForm.setBounds(12, 418, 862, 128);
		window.add(messageForm);
		messageForm.setLayout(null);
		
		messageFormSubjectField = new JTextField();
		messageFormSubjectField.setEnabled(false);
		messageFormSubjectField.setBounds(12, 12, 373, 19);
		messageForm.add(messageFormSubjectField);
		messageFormSubjectField.setColumns(10);
		
		JTextArea messageFormBodyField = new JTextArea();
		messageFormBodyField.setEnabled(false);
		messageFormBodyField.setBounds(12, 43, 692, 73);
		messageForm.add(messageFormBodyField);
		
		JLabel messageFormUserLabel = new JLabel("Send to :");
		messageFormUserLabel.setEnabled(false);
		messageFormUserLabel.setBounds(403, 14, 60, 15);
		messageForm.add(messageFormUserLabel);
		
		messageFormUserField = new JTextField();
		messageFormUserField.setEnabled(false);
		messageFormUserField.setBounds(481, 12, 223, 19);
		messageForm.add(messageFormUserField);
		messageFormUserField.setColumns(10);
		
		JLabel messageFormInfoLabel = new JLabel("");
		messageFormInfoLabel.setForeground(Color.RED);
		messageFormInfoLabel.setEnabled(false);
		messageFormInfoLabel.setBounds(722, 97, 128, 19);
		messageForm.add(messageFormInfoLabel);
		
		// Components with events
		JButton messageFormEventButton = new JButton("Send message");
		messageFormEventButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				messageFormInfoLabel.setText("");
				for(Talkative t : Talkative.talkatives) {
					if(t.connection.user.equals(messageFormUserField.getText())) {
						Person.connected.emit(new Message(messageFormSubjectField.getText(),messageFormBodyField.getText(),Person.connected,t));
						messageFormInfoLabel.setText("Message sent !");
						DefaultListModel<Message> model = new DefaultListModel<Message>();
						for(Message m : Person.connected.messageBox)
							model.addElement(m);
						messageList.setModel(model);
						break;
					}
				}
				if(messageFormInfoLabel.getText()=="") {
					for(Concierge c : Concierge.concierges) {
						if(c.connection.user.equals(messageFormUserField.getText())) {
							Person.connected.emit(new Message(messageFormSubjectField.getText(),messageFormBodyField.getText(),Person.connected,c));
							messageFormInfoLabel.setText("Message sent !");
							DefaultListModel<Message> model = new DefaultListModel<Message>();
							for(Message m : Person.connected.messageBox)
								model.addElement(m);
							messageList.setModel(model);
							break;
						}
					}
				}
				if(messageFormInfoLabel.getText()=="") {
					messageFormInfoLabel.setText("User not found !");
				}
			}
		});
		messageFormEventButton.setEnabled(false);
		messageFormEventButton.setBounds(716, 9, 134, 25);
		messageForm.add(messageFormEventButton);
		
		// =================== CONNECTION ==========================
		// Components
		JPanel connection = new JPanel();
		connection.setBorder(new LineBorder(new Color(0, 0, 0)));
		connection.setBounds(12, 12, 300, 100);
		window.add(connection);
		connection.setLayout(null);
		
		JLabel connectionUserLabel = new JLabel("User :");
		connectionUserLabel.setBounds(12, 12, 60, 15);
		connection.add(connectionUserLabel);
		
		connectionUserField = new JTextField();
		connectionUserField.setBounds(108, 10, 180, 19);
		connection.add(connectionUserField);
		connectionUserField.setColumns(10);
		
		JLabel connectionPasswordLabel = new JLabel("Password :");
		connectionPasswordLabel.setBounds(12, 37, 80, 15);
		connection.add(connectionPasswordLabel);
		
		connectionPasswordField = new JTextField();
		connectionPasswordField.setBounds(108, 35, 180, 19);
		connection.add(connectionPasswordField);
		connectionPasswordField.setColumns(10);
		
		JLabel connectionInfoLabel = new JLabel("");
		connectionInfoLabel.setForeground(Color.RED);
		connectionInfoLabel.setBounds(136, 63, 147, 15);
		connection.add(connectionInfoLabel);
		
		// Components with events
		JButton connectionEventButton = new JButton("Connect");
		connectionEventButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				connectionInfoLabel.setText("");
				if(Person.connected==null) {
					for(Person p : Person.people) {
						if(p.connect(connectionUserField.getText(),connectionPasswordField.getText())) {
							Person.connected=p;
							Person.connected.connection.emitOn();
							// Connection
							connectionUserLabel.setEnabled(false);
							connectionUserField.setEnabled(false);
							connectionPasswordLabel.setEnabled(false);
							connectionPasswordField.setEnabled(false);
							connectionPasswordField.setText("");
							connectionEventButton.setText("Disconnect");
							// User
							for(Concierge c : Concierge.concierges) {
								if(c.connect(Person.connected.connection.user,Person.connected.connection.password)) {
									userUserLabel.setEnabled(true);
									userUserField.setEnabled(true);
									userPasswordLabel.setEnabled(true);
									userPasswordField.setEnabled(true);
									userConciergeField.setEnabled(true);
									userInfoLabel.setEnabled(true);
									userCreateEventButton.setEnabled(true);
									userDeleteEventButton.setEnabled(true);
									userUpdateEventButton.setEnabled(true);
									break;
								}
							}
							// Message
							DefaultListModel<Message> model = new DefaultListModel<Message>();
							for(Message m : Person.connected.messageBox)
								model.addElement(m);
							messageList.setModel(model);
							messageList.setEnabled(true);
							messageSubjectLabel.setEnabled(true);
							messageAuthorLabel.setEnabled(true);
							messageBodyLabel.setEnabled(true);
							// MessageForm
							messageFormSubjectField.setEnabled(true);
							messageFormBodyField.setEnabled(true);
							messageFormEventButton.setEnabled(true);
							messageFormUserLabel.setEnabled(true);
							messageFormUserField.setEnabled(true);
							messageFormInfoLabel.setEnabled(true);
							break;
						}
					}
					if(Person.connected==null)
						connectionInfoLabel.setText("Bad information !");
				} else {
					Person.connected.connection.emitOff();
					Person.connected=null;
					// Connection
					connectionUserLabel.setEnabled(true);
					connectionUserField.setEnabled(true);
					connectionPasswordLabel.setEnabled(true);
					connectionPasswordField.setEnabled(true);
					connectionEventButton.setText("Connect");
					// User
					userUserLabel.setEnabled(false);
					userUserField.setEnabled(false);
					userUserField.setText("");
					userPasswordLabel.setEnabled(false);
					userPasswordField.setEnabled(false);
					userPasswordField.setText("");
					userConciergeField.setEnabled(false);
					userInfoLabel.setEnabled(false);
					userInfoLabel.setText("");
					userCreateEventButton.setEnabled(false);
					userDeleteEventButton.setEnabled(false);
					userUpdateEventButton.setEnabled(false);
					// Message
					messageList.setModel(new DefaultListModel<Message>());
					messageList.setEnabled(false);
					messageSubjectLabel.setEnabled(false);
					messageSubjectLabel.setText("");
					messageAuthorLabel.setEnabled(false);
					messageAuthorLabel.setText("");
					messageBodyLabel.setEnabled(false);
					messageBodyLabel.setText("");
					// MessageForm
					messageFormSubjectField.setEnabled(false);
					messageFormSubjectField.setText("");
					messageFormBodyField.setEnabled(false);
					messageFormBodyField.setText("");
					messageFormUserLabel.setEnabled(false);
					messageFormUserField.setEnabled(false);
					messageFormUserField.setText("");
					messageFormEventButton.setEnabled(false);
					messageFormInfoLabel.setEnabled(true);
					messageFormInfoLabel.setText("");
				}
			}
		});
		connectionEventButton.setBounds(12, 64, 106, 25);
		connection.add(connectionEventButton);
	}
}
