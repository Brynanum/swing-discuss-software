/**
 * Launch the program.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
public class Main {
	/**
 	* Entry of the program. Initialize data, and launch the graphical interface.
 	*
 	* @param  args Given arguments at the program (useless for this function)
 	* @see         Concierge
 	* @see         Talkative
 	* @see         GUI
 	*/
	public static void main(String args[]) {
		Concierge.main(args);
		Talkative.main(args);
		GUI.main(args);
	}
}
