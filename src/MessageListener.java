/**
 * Determine if a class can receive a message.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
public interface MessageListener {
	/**
 	* Determine if a class can receive a message.
 	*
 	* @param  message  The message to receive.
 	* @see             Person
 	* @see             Talkative
 	* @see             Concierge
 	*/
	public void receive(Message message);
}
