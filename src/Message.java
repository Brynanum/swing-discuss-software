/**
 * Represent a message.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
 public class Message {
	/**
	* The subject / title of the message.
	*/
	public String subject;
	/**
	* The body / content of the message.
	*/
	public String body;
	/**
	* The Person who sent the message.
	*/
	public Person from;
	/**
	* The Person who received the message.
	*/
	public Person to;

	/**
 	* Construct a Message.
 	*
 	* @param  subject  The title / subject of the message
 	* @param  body 	   The content / body of the message
 	* @param  from     Person who sent the message
 	* @param  to       Person who received the message
 	* @see    MessageEvent
 	* @see    MessageListener
 	* @see    Person
 	*/
	public Message(String subject,String body,Person from,Person to) {
		this.subject=subject;
		this.body=body;
		this.from=from;
		this.to=to;
	}
	
	/**
	* Return the String value of the entity.
	*
	* @return              The subject of the message
	* @see     Object
	*/
	@Override public String toString() {
		return this.subject;
	}
}
