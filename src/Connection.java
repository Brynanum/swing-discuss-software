/**
 * Represent a possibility to connect.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
public class Connection implements MessageEvent {
	/**
    * The identifiant of the connection.
    */
	public String user;
	/**
    * The password of the connection.
    */
	public String password;
	
	/**
 	* Construct a connection.
 	*
 	* @param  user     The identifiant of the connection
 	* @param  password The password of the connection
 	* @see             Person
 	*/
	public Connection(String user,String password) {
		this.user=user;
		this.password=password;
	}

	/**
 	* Give the autorization to connect.
 	*
 	* @param  user     The identifiant of the connection
 	* @param  password The password of the connection
 	* @return          The autorization in a boolean format
 	* @see             Person
 	*/
	public boolean connect(String user, String password) {
		return this.user.equals(user) && this.password.equals(password);
	}

	/**
 	* Send a message of connection.
 	*
 	* @param  message the message to send
 	*/
	public void emit(Message message) {
		message.to.receive(message);
	}

	/**
 	* Notify the concierge that this entity is connecting.
 	*
 	* @see  Concierge
 	*/
	public void emitOn() {
		this.emit(new Message(this.user+" connected",this.user+" is connected",Concierge.concierges.get(0),Concierge.concierges.get(0)));
	}

	/**
 	* Notify the concierge that this entity is disconnecting.
 	*
 	* @see  Concierge
 	*/
	public void emitOff() {
		this.emit(new Message(this.user+" disconnected",this.user+" is disconnected",Concierge.concierges.get(0),Concierge.concierges.get(0)));
	}
}
