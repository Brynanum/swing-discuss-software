import java.util.ArrayList;

/**
 * Represent a Concierge.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
public class Concierge extends Person {
	/**
    * Contain the list of real Concierge persons.
    */
	public static ArrayList<Concierge> concierges = new ArrayList<Concierge>();
	
	/**
 	* Construct one Concierge person, to use it in the example program.
 	* <p>concierge1 & concierge1password</p>
 	*
 	* @param  args Given arguments at the program (useless for this function)
 	* @see         Main
 	* @see         Person
 	*/
	public static void main(String args[]) {
		new Concierge("concierge1","concierge1password",true);
	}

	/**
 	* Construct a temporary Concierge person.
 	*
 	* @param  user     The pseudo of the Talkative person
 	* @param  password The password of the Talkative person
 	* @see             Person
 	*/
	public Concierge(String user, String password) {
		super(user,password);
	}
	/**
 	* Construct a Talkative person.
 	*
 	* @param  user     The pseudo of the Talkative person
 	* @param  password The password of the Talkative person
 	* @param  isReal   Determine if it is a temporary or real Talkative person
 	* @see             Person
 	*/
	public Concierge(String user, String password, boolean isReal) {
		super(user,password,isReal);
		if(isReal) {
			Concierge.concierges.add(this);
		}
	}

	/**
 	* When a Concierge person receives a message, he sends it to everyone.
 	*
 	* @param  message  The received message, to send to everyone
 	* @see             MessageListener
 	* @see             Person
 	*/
	@Override public void receive(Message message) {
		super.receive(message);
		for(Talkative talkative : Talkative.talkatives) {
			this.emit(new Message(message.subject,message.body,this,talkative));
		}
	}
}
