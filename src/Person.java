import java.util.ArrayList;

/**
 * Represent a Person.
 * 
 * @author Bryan Fauquembergue (Brynanum)
 */
public class Person implements MessageEvent, MessageListener {
	/**
	* The connection access of the Person.
	*/
	public Connection connection=null;
	/**
	* The messages sent to the Person.
	*/
	public ArrayList<Message> messageBox = new ArrayList<Message>();
	/**
	* The list of real Person.
	*/
	public static ArrayList<Person> people = new ArrayList<Person>();
	/**
	* The connected Person.
	*/
	public static Person connected=null;

	/**
 	* Construct a temporary Person.
 	*
 	* @param  user     The pseudo of the Person
 	* @param  password The password of the Person
 	*/
	public Person(String user,String password) {
		this.connection = new Connection(user,password);
	}
	/**
 	* Construct a Person.
 	*
 	* @param  user     The pseudo of the Person
 	* @param  password The password of the Person
 	* @param  isReal   Determine if it is a temporary or real Talkative person
 	*/
	public Person(String user,String password,boolean isReal) {
		this(user,password);
		if(isReal) {
			Person.people.add(this);
		}
	}
	
	/**
 	* Connect the Person.
 	*
 	* @param  user     The pseudo of the Person
 	* @param  password The password of the Person
 	* @see             Connection
 	*/
	public boolean connect(String user, String password) {
		return this.connection.connect(user,password);
	}

	/**
 	* Send a message.
 	*
 	* @param  message  The message to send.
 	* @see             Message
 	* @see             MessageEvent
 	*/
	public void emit(Message message) {
		message.to.receive(message);
	}

	/**
 	* Receive a message.
 	*
 	* @param  message  The message to receive.
 	* @see             Message
 	* @see             MessageListener
 	*/
	public void receive(Message message) {
		this.messageBox.add(message);
	}

	/**
 	* Update a Person.
 	*
 	* @param  user     The new pseudo of the Person
 	* @param  password The new password of the Person
 	* @see             Connection
 	*/
	public void modify(String user, String password) {
		this.connection.user=user;
		this.connection.password=password;
	}
}
