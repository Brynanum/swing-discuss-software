# Java Swing : Dialog program between users
![Illustration of the project](https://gitlab.com/Brynanum/igi641/raw/master/pictures/5.png)
## Introduction
A system of discussion between people in Java. This small project can help you to understand the basis of an graphical interface in Java Swing, without follow a tutorial.

## How can I use it ?
Execute the Main class. In a Linux shell, after downloading the project and into the src folder :
```bash
java Main
```

## Documentation
You have the possibility to see the Javadoc of the project in the doc folder, and preview it on https://htmlpreview.github.io .

## Required Java libraries
This project needs Swing librairies (with AWT), in complement of native libraries.

# Author
Bryan Fauquembergue

# Improvement
This project will not have some amelioration. However, you can fork it and share your own ameliorations, while you expect the license.

# User guide
By default, three Talkative people and one Concierge administrator are created, with these following information :
* `talkative1` --> talkative1password
* `talkative2` --> talkative2password
* `talkative3` --> talkative3password
* `concierge1` --> concierge1password
Let you connect with `talkative1` information. When someone is connecting, a notification is realized to everyone : you can click on the title message to see the complete message, such as the following picture.
![How can I use it ? - Step 1](https://gitlab.com/Brynanum/igi641/raw/master/pictures/1.png)
We will send a message to `talkative2`. Fill fields in the bottom form, and click on "Send a message". An error appears ! Indeed, the project is configured : talkative people can only send a message to a concierge. Don't worry : this is able to change in the code.
![How can I use it ? - Step 2](https://gitlab.com/Brynanum/igi641/raw/master/pictures/2.png)
So, we will send our message to `concierge1`. Modify the destinary, and send again the message. If you take a look on the list of messages, you can noticed that your sent message is here... Why ? Because there is an other configuration in the code : when a concierge receive a message, he sends it to everyone. Don't worry again : this can also be changed in the code.
![How can I use it ? - Step 3](https://gitlab.com/Brynanum/igi641/raw/master/pictures/3.png)
Disconnect, and connect again as `concierge1`. Such as any concierge, you have an accessible user manager at the top right of the window. You are able to create a new user, update an existing user or delete him, according to the user name. Try to use this form if you want, and to connect with these (new) users to see the result.
![How can I use it ? - Step 1](https://gitlab.com/Brynanum/igi641/raw/master/pictures/4.png)
Don't hesitate to fork this project, and changed all : the design, the restriction / permission on users, add a filter function on messages, etc.
![How can I use it ? - Step 1](https://gitlab.com/Brynanum/igi641/raw/master/pictures/5.png)
